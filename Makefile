EH_VERSION = v0.16.0

COMPOSE_RUN_GO = docker compose run --rm golang

prepare:
	docker compose pull
.PHONY: prepare

init:
	$(COMPOSE_RUN_GO) make _init
.PHONY: init

upgrade:
	$(COMPOSE_RUN_GO) make _upgrade
.PHONY: upgrade

test: services test_postgres test_mysql

test_postgres:
	$(COMPOSE_RUN_GO) make _test_postgres
.PHONY: test_postgres

test_mysql:
	$(COMPOSE_RUN_GO) make _test_mysql
.PHONY: test_mysql

services:
	docker compose up --detach --wait postgres mysql
.PHONY: services

services_log:
	docker compose logs -f postgres
.PHONY: services_log

clean:
	docker compose down --remove-orphans --volumes
.PHONY: clean

# Helpers
shellGo:
	$(COMPOSE_RUN_GO) sh
.PHONY: shelllGo

# Internal targets
_init:
	go mod download

_upgrade:
	go get -u -t ./...
	go get -u github.com/looplab/eventhorizon@$(EH_VERSION)
	go mod tidy

_test_postgres:
	RUN=postgres go test ./... -cover
	RUN=postgres go test -bench=.

_test_mysql:
	RUN=mysql go test ./...

gitTag:
	-git tag -d $(EH_VERSION)
	-git push origin :refs/tags/$(EH_VERSION)
	git tag $(EH_VERSION)
	git push origin $(EH_VERSION)
