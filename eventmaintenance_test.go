// Copyright (c) 2024 - The Event Horizon GORM authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ehgorm

import (
	"context"
	"fmt"
	"github.com/looplab/eventhorizon/eventstore"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"testing"
)

func TestEventStoreMaintenancePostgresIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests")
	}

	run := os.Getenv("RUN")
	if run != "" && run != "postgres" {
		t.Skip(fmt.Sprintf("only running tests for %v", run))
	}

	host := os.Getenv("PG_DB_HOST")
	if host == "" {
		host = "localhost"
	}

	port := os.Getenv("PG_DB_PORT")
	if port == "" {
		port = "5432"
	}

	username := os.Getenv("PG_DB_USER")
	if username == "" {
		username = "postgres"
	}

	password := os.Getenv("PG_DB_PASSWORD")
	if password == "" {
		password = "password"
	}

	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=eventhorizon port=%v sslmode=disable TimeZone=Australia/Melbourne", host, username, password, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal(err)
	}

	store, err := NewEventStore(db)
	defer store.Close()

	if err != nil {
		t.Fatal("there should be no error:", err)
	}

	if store == nil {
		t.Fatal("there should be a store")
	}

	err = store.Clear(context.Background())
	if err != nil {
		t.Fatal("there should be no error")
	}

	store, _ = NewEventStore(db)

	eventstore.MaintenanceAcceptanceTest(t, store, store, context.Background())
}

func TestEventStoreMaintenanceMysqlIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests")
	}

	host := os.Getenv("MY_DB_HOST")
	if host == "" {
		host = "localhost"
	}

	port := os.Getenv("MY_DB_PORT")
	if port == "" {
		port = "3306"
	}

	username := os.Getenv("MY_DB_USER")
	if username == "" {
		username = "mysql"
	}

	password := os.Getenv("MY_DB_PASSWORD")
	if password == "" {
		password = "mysql"
	}

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/eventhorizon?charset=utf8mb4&parseTime=True&loc=UTC", username, password, host, port)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal(err)
	}

	store, err := NewEventStore(db)
	defer store.Close()

	if err != nil {
		t.Fatal("there should be no error:", err)
	}

	if store == nil {
		t.Fatal("there should be a store")
	}

	err = store.Clear(context.Background())
	if err != nil {
		t.Fatal("there should be no error")
	}

	store, _ = NewEventStore(db)

	eventstore.MaintenanceAcceptanceTest(t, store, store, context.Background())
}
