// Copyright (c) 2024 - The Event Horizon GORM authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ehgorm

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	eh "github.com/looplab/eventhorizon"
	"github.com/looplab/eventhorizon/mocks"
	"github.com/looplab/eventhorizon/repo"
	"github.com/looplab/eventhorizon/uuid"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"reflect"
	"testing"
	"time"
)

func TestRepoPostgresIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests")
	}

	run := os.Getenv("RUN")
	if run != "" && run != "postgres" {
		t.Skip(fmt.Sprintf("only running tests for %v", run))
	}

	// time.Unix() is used in the gorm/pgx which trips up acceptance testing in EH
	// setting time.Local to nil ensures that time.Date() with UTC and time.Unix have the same location
	oldTimeLocal := time.Local
	time.Local = nil

	host := os.Getenv("PG_DB_HOST")
	if host == "" {
		host = "localhost"
	}

	port := os.Getenv("PG_DB_PORT")
	if port == "" {
		port = "5432"
	}

	username := os.Getenv("PG_DB_USER")
	if username == "" {
		username = "postgres"
	}

	password := os.Getenv("PG_DB_PASSWORD")
	if password == "" {
		password = "password"
	}

	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=eventhorizon port=%v sslmode=disable TimeZone=UTC", host, username, password, port)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal(err)
	}

	r, err := NewRepo(db)
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if r == nil {
		t.Error("there should be a repository")
	}

	err = r.SetEntityFactory(func() eh.Entity {
		return &mocks.Model{}
	})
	if err != nil {
		t.Error("there should be no error:", err)
	}

	err = r.Clear()
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if r.InnerRepo(context.Background()) != nil {
		t.Error("the inner repo should be nil")
	}

	repo.AcceptanceTest(t, r, context.Background())
	extraRepoTests(t, r)

	if err := r.Close(); err != nil {
		t.Error("there should be no error:", err)
	}

	// Reset local time
	time.Local = oldTimeLocal
}

func TestRepoMysqlIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests")
	}

	run := os.Getenv("RUN")
	if run != "" && run != "mysql" {
		t.Skip(fmt.Sprintf("only running tests for %v", run))
	}

	host := os.Getenv("MY_DB_HOST")
	if host == "" {
		host = "localhost"
	}

	port := os.Getenv("MY_DB_PORT")
	if port == "" {
		port = "3306"
	}

	username := os.Getenv("MY_DB_USER")
	if username == "" {
		username = "mysql"
	}

	password := os.Getenv("MY_DB_PASSWORD")
	if password == "" {
		password = "mysql"
	}

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/eventhorizon?charset=utf8mb4&parseTime=True&loc=UTC", username, password, host, port)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal(err)
	}

	r, err := NewRepo(db)
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if r == nil {
		t.Error("there should be a repository")
	}

	err = r.SetEntityFactory(func() eh.Entity {
		return &mocks.Model{}
	})
	if err != nil {
		t.Error("there should be no error:", err)
	}

	err = r.Clear()
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if r.InnerRepo(context.Background()) != nil {
		t.Error("the inner repo should be nil")
	}

	repo.AcceptanceTest(t, r, context.Background())
	extraRepoTests(t, r)

	if err := r.Close(); err != nil {
		t.Error("there should be no error:", err)
	}
}

func extraRepoTests(t *testing.T, r *Repo) {
	ctx := context.Background()

	// FindCustomIter Empty result
	iter, err := r.FindCustomIter(func(db *gorm.DB) *gorm.DB {
		return db.Where("content = ?", "modelCustom")
	})

	if err != nil {
		t.Error("there should be no error:", err)
	}

	if iter.Next(ctx) == true {
		t.Error("the iterator should not have results")
	}

	err = iter.Close(ctx)
	if err != nil {
		t.Error("there should be no error:", err)
	}

	// Insert a custom item.
	modelCustom := &mocks.Model{
		ID:        uuid.New(),
		Content:   "modelCustom",
		CreatedAt: time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC),
	}

	if err := r.Save(ctx, modelCustom); err != nil {
		t.Error("there should be no error:", err)
	}

	// FindCustom by content
	result, err := r.FindCustom(func(db *gorm.DB) *gorm.DB {
		return db.Where("content = @content", sql.Named("content", "modelCustom"))
	})

	if len(result) != 1 {
		t.Error("there should be one item:", len(result))
	}

	if !reflect.DeepEqual(result[0], modelCustom) {
		t.Error("the item should be correct:", modelCustom)
	}

	// FindCustom with no query.
	repoErr := &eh.RepoError{}

	_, err = r.FindCustom(func(db *gorm.DB) *gorm.DB { return nil })
	if !errors.As(err, &repoErr) || !errors.Is(repoErr.Err, ErrNoDB) {
		t.Error("there should be a invalid query error:", err)
	}

	var count int64
	// FindCustom with query execution in the callback.
	repoErr = &eh.RepoError{}

	_, err = r.FindCustom(func(db *gorm.DB) *gorm.DB {
		res := db.Count(&count)
		if res.Error != nil {
			t.Error("there should be no error:", res.Error)
		}
		// Be sure to return nil to not execute the query again in FindCustom
		return nil
	})
	if !errors.As(err, &repoErr) || !errors.Is(repoErr.Err, ErrNoDB) {
		t.Error("there should be a invalid query error:", err)
	}

	if count != 2 {
		t.Error("the count should be correct:", count)
	}

	modelCustom2 := &mocks.Model{
		ID:        uuid.New(),
		Content:   "modelCustom2",
		CreatedAt: time.Date(2024, time.November, 10, 23, 0, 0, 0, time.UTC),
	}

	if err = r.DB(func(db *gorm.DB) error {
		return db.Create(&modelCustom2).Error
	}); err != nil {
		t.Error("there should be no error:", err)
	}

	model, err := r.Find(ctx, modelCustom2.ID)
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if !reflect.DeepEqual(model, modelCustom2) {
		t.Error("the item should be correct:", model)
	}

	// FindCustomIter by content
	iter, err = r.FindCustomIter(func(db *gorm.DB) *gorm.DB {
		return db.Where("content = ?", "modelCustom")
	})

	if err != nil {
		t.Error("there should be no error:", err)
	}

	if iter.Next(ctx) != true {
		t.Error("the iterator should have results")
	}

	if !reflect.DeepEqual(iter.Value(), modelCustom) {
		t.Error("the item should be correct:", modelCustom)
	}

	if iter.Next(ctx) == true {
		t.Error("the iterator should have no results")
	}

	err = iter.Close(ctx)
	if err != nil {
		t.Error("there should be no error:", err)
	}

	// FindOneCustom by content.
	singleResult, err := r.FindOneCustom(func(db *gorm.DB) *gorm.DB {
		return db.Where("content = ?", "modelCustom")
	})
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if !reflect.DeepEqual(singleResult, modelCustom) {
		t.Error("the item should be correct:", singleResult)
	}

	_, err = r.FindOneCustom(func(db *gorm.DB) *gorm.DB {
		return db.Where("content = ?", "modelCustomFoo")
	})

	if !errors.As(err, &repoErr) || !errors.Is(repoErr.Err, eh.ErrEntityNotFound) {
		t.Error("there should be a entity not found error:", err)
	}
}

func TestIntoRepoIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests")
	}

	run := os.Getenv("RUN")
	if run != "" && run != "postgres" {
		t.Skip(fmt.Sprintf("only running tests for %v", run))
	}

	host := os.Getenv("PG_DB_HOST")
	if host == "" {
		host = "localhost"
	}

	port := os.Getenv("PG_DB_PORT")
	if port == "" {
		port = "5432"
	}

	username := os.Getenv("PG_DB_USER")
	if username == "" {
		username = "postgres"
	}

	password := os.Getenv("PG_DB_PASSWORD")
	if password == "" {
		password = "password"
	}

	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=eventhorizon port=%v sslmode=disable TimeZone=UTC", host, username, password, port)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal(err)
	}

	inner, err := NewRepo(db)
	defer inner.Close()
	if err != nil {
		t.Error("there should be no error:", err)
	}

	if inner == nil {
		t.Error("there should be a repository")
	}

	err = inner.SetEntityFactory(func() eh.Entity {
		return &mocks.Model{}
	})
	if err != nil {
		t.Error("there should be no error:", err)
	}

	err = inner.Clear()
	if err != nil {
		t.Error("there should be no error:", err)
	}

	outer := &mocks.Repo{ParentRepo: inner}
	if r := IntoRepo(context.Background(), outer); r != inner {
		t.Error("the repository should be correct:", r)
	}
}
