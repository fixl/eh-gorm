[![pipeline status](https://gitlab.com/fixl/eh-gorm/badges/master/pipeline.svg)](https://gitlab.com/fixl/eh-gorm/commits/master)
[![coverage report](https://gitlab.com/fixl/eh-gorm/badges/master/coverage.svg)](https://gitlab.com/fixl/eh-gorm/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/fixl/eh-gorm?status.svg)](https://godoc.org/gitlab.com/fixl/eh-gorm)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/fixl/eh-gorm)](https://goreportcard.com/report/gitlab.com/fixl/eh-gorm)

# eh-gorm

eh-gorm provides datastores leveraging [GORM] for the [Event Horizon] CQRS/ES toolkit for Go. This means you can use
any underlying database that is supported by GORM:

- PostgreSQL
- MySQL
- SQL Server
- TiDB
- Clickhouse
- SQLite

[GORM]: https://gorm.io/
[Event Horizon]: https://github.com/looplab/eventhorizon

## Usage

See the Event Horizon example folder for a few examples to get you started and replace the storage drivers (event
store and/or repo)

## Setup

eh-gorm automatically creates the tables it requires.

## GORM Connection

Check out GORM's [Connecting to a Database] section to configure a repo or eventstore:

**Repo**
```go
// Mysql
import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)
dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
if err !nil {
	// handle error
}

repo, err := NewRepo(db)

// Postgres
import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

dsn := "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
if err !nil {
	// handle error
}

repo, err := NewRepo(db)
```

**Eventstore**
```go
// Mysql
import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)
dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
if err !nil {
	// handle error
}

repo, err := NewEventStore(db)

// Postgres
import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

dsn := "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
if err !nil {
	// handle error
}

repo, err := NewEventStore(db)
```

[Connecting to a Database]: https://gorm.io/docs/connecting_to_the_database.html

## Development

To develop eh-gorm you need to have Docker and Docker Compose installed.

To start all required services and run all tests, simply run make:

```bash
make test
```

To manually start services for local development outside Docker:

```bash
make services
```

You can then run either one of the following:

```bash
make test
make _test
go test ./...
```

or use your favourite IDE to run your tests with a Cloud Firestore emulator
in the background.