// Copyright (c) 2024 - The Event Horizon GORM authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ehgorm

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	eh "github.com/looplab/eventhorizon"
	"github.com/looplab/eventhorizon/uuid"
	"gorm.io/gorm"
)

var (
	// ErrModelNotSet is when a model factory is not set on the Repo.
	ErrModelNotSet = errors.New("model not set")

	// ErrNoDB is when a provided callback returns a nil db
	ErrNoDB = errors.New("no db")
)

type Repo struct {
	db        *gorm.DB
	newEntity func() eh.Entity
}

func NewRepo(db *gorm.DB) (*Repo, error) {
	if db == nil {
		return nil, fmt.Errorf("missing db")
	}

	return &Repo{
		db: db,
	}, nil
}

// SetEntityFactory sets a factory function that create concrete entity types.
func (r *Repo) SetEntityFactory(f func() eh.Entity) error {
	r.newEntity = f

	m := f()

	if err := r.db.AutoMigrate(&m); err != nil {
		return fmt.Errorf("could not create entity: %w", err)
	}

	return nil
}

// Clear clears the read model database
func (r *Repo) Clear() error {
	m := r.newEntity()
	if err := r.db.Migrator().DropTable(&m); err != nil {
		return fmt.Errorf("could not drop entity table: %w", err)
	}

	return r.SetEntityFactory(r.newEntity)
}

// InnerRepo implements the InnerRepo method of the eventhorizon.ReadRepo interface.
func (r *Repo) InnerRepo(ctx context.Context) eh.ReadRepo {
	return nil
}

// IntoRepo tries to convert a eh.ReadRepo into a Repo by recursively looking at
// inner repos. Returns nil if none was found.
func IntoRepo(ctx context.Context, repo eh.ReadRepo) *Repo {
	if repo == nil {
		return nil
	}

	if r, ok := repo.(*Repo); ok {
		return r
	}

	return IntoRepo(ctx, repo.InnerRepo(ctx))
}

// Find implements the Find method of the eventhorizon.ReadRepo interface.
func (r *Repo) Find(_ context.Context, id uuid.UUID) (eh.Entity, error) {
	if r.newEntity == nil {
		return nil, &eh.RepoError{
			Err:      ErrModelNotSet,
			Op:       eh.RepoOpFind,
			EntityID: id,
		}
	}

	entity := r.newEntity()
	if err := r.db.Take(entity, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = eh.ErrEntityNotFound
		}

		return nil, &eh.RepoError{
			Err: err,
			Op:  eh.RepoOpFind,
		}
	}

	return entity, nil
}

// FindAll implements the FindAll method of the eventhorizon.ReadRepo interface.
func (r *Repo) FindAll(ctx context.Context) ([]eh.Entity, error) {
	if r.newEntity == nil {
		return nil, &eh.RepoError{
			Err: ErrModelNotSet,
			Op:  eh.RepoOpFind,
		}
	}

	m := r.newEntity()

	var entities []eh.Entity

	rows, err := r.db.Model(&m).Rows()
	defer rows.Close()

	if err != nil {
		return nil, &eh.RepoError{
			Err: fmt.Errorf("could not find: %w", err),
			Op:  eh.RepoOpFindAll,
		}
	}

	for rows.Next() {
		entity := r.newEntity()
		if err = r.db.ScanRows(rows, entity); err != nil {
			return nil, &eh.RepoError{
				Err: fmt.Errorf("could not process row: %w", err),
				Op:  eh.RepoOpFindAll,
			}
		}

		entities = append(entities, entity)
	}

	return entities, nil
}

// The iterator is not thread safe
type iter struct {
	db        *gorm.DB
	rows      *sql.Rows
	data      eh.Entity
	newEntity func() eh.Entity
	scanErr   error
}

func (i *iter) Next(_ context.Context) bool {
	if !i.rows.Next() {
		return false
	}

	entity := i.newEntity()
	i.scanErr = i.db.ScanRows(i.rows, entity)
	i.data = entity

	return true
}

func (i *iter) Value() interface{} {
	return i.data
}

func (i *iter) Close(_ context.Context) error {
	if err := i.rows.Close(); err != nil {
		return err
	}

	return i.scanErr
}

// FindCustomIter returns an eh.Iter you can use to stream results of very large datasets.
// The db parameter has the model set appropriately as it is configured on the repo.
func (r *Repo) FindCustomIter(f func(db *gorm.DB) *gorm.DB) (eh.Iter, error) {
	if r.newEntity == nil {
		return nil, &eh.RepoError{
			Err: ErrModelNotSet,
			Op:  eh.RepoOpFindQuery,
		}
	}

	res := f(r.db.Model(r.newEntity()))
	if res == nil {
		return nil, &eh.RepoError{
			Err: ErrNoDB,
			Op:  eh.RepoOpFindQuery,
		}
	}

	rows, err := res.Rows()
	if err != nil {
		return nil, &eh.RepoError{
			Err: fmt.Errorf("could not find: %w", err),
			Op:  eh.RepoOpFindQuery,
		}
	}

	return &iter{
		rows:      rows,
		db:        r.db,
		newEntity: r.newEntity,
	}, nil
}

// FindCustom uses a callback to specify a custom query for returning models.
// It can be used to do queries that do not map to the model by executing
// the query in the callback and returning nil to block a second execution of
// the same query in FindCustom. Expect a ErrNoDB if returning a nil DB from the
// callback.
func (r *Repo) FindCustom(f func(db *gorm.DB) *gorm.DB) ([]interface{}, error) {
	if r.newEntity == nil {
		return nil, &eh.RepoError{
			Err: ErrModelNotSet,
			Op:  eh.RepoOpFindQuery,
		}
	}

	res := f(r.db.Model(r.newEntity()))
	if res == nil {
		return nil, &eh.RepoError{
			Err: ErrNoDB,
			Op:  eh.RepoOpFindQuery,
		}
	}

	rows, err := res.Rows()
	if err != nil {
		return nil, &eh.RepoError{
			Err: fmt.Errorf("could not find: %w", err),
			Op:  eh.RepoOpFindQuery,
		}
	}

	var result []interface{}
	for rows.Next() {
		entity := r.newEntity()
		if err = r.db.ScanRows(rows, entity); err != nil {
			return nil, &eh.RepoError{
				Err: fmt.Errorf("could not process row: %w", err),
				Op:  eh.RepoOpFindQuery,
			}
		}
		result = append(result, entity)
	}

	if err = rows.Close(); err != nil {
		return nil, &eh.RepoError{
			Err: fmt.Errorf("could not close rows: %w", err),
			Op:  eh.RepoOpFindQuery,
		}
	}

	return result, nil
}

// FindOneCustom uses a callback to specify a custom query for returning an entity.
func (r *Repo) FindOneCustom(f func(db *gorm.DB) *gorm.DB) (eh.Entity, error) {
	if r.newEntity == nil {
		return nil, &eh.RepoError{
			Err: ErrModelNotSet,
			Op:  eh.RepoOpFindQuery,
		}
	}

	res := f(r.db.Model(r.newEntity()))

	entity := r.newEntity()
	if err := res.Take(entity).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = eh.ErrEntityNotFound
		}

		return nil, &eh.RepoError{
			Err: err,
			Op:  eh.RepoOpFindQuery,
		}
	}

	return entity, nil
}

// Save implements the Save method of the eventhorizon.WriteRepo interface.
func (r *Repo) Save(ctx context.Context, entity eh.Entity) error {
	id := entity.EntityID()
	if id == uuid.Nil {
		return &eh.RepoError{
			Err: fmt.Errorf("missing entity ID"),
			Op:  eh.RepoOpSave,
		}
	}

	if err := r.db.Save(entity).Error; err != nil {
		return &eh.RepoError{
			Err:      fmt.Errorf("could not save/update: %w", err),
			Op:       eh.RepoOpSave,
			EntityID: id,
		}
	}

	return nil
}

// Remove implements the Remove method of the eventhorizon.WriteRepo interface.
func (r *Repo) Remove(ctx context.Context, id uuid.UUID) error {
	m := r.newEntity()

	res := r.db.Delete(m, id)
	if res.Error != nil {
		return &eh.RepoError{
			Err:      res.Error,
			Op:       eh.RepoOpRemove,
			EntityID: id,
		}
	}

	// Nothing was deleted
	if res.RowsAffected == 0 {
		return &eh.RepoError{
			Err:      eh.ErrEntityNotFound,
			Op:       eh.RepoOpRemove,
			EntityID: id,
		}
	}

	return nil
}

// Close implements the Close method of the eventhorizon.WriteRepo interface.
func (r *Repo) Close() error {
	db, err := r.db.DB()
	if err != nil {
		return fmt.Errorf("could not retrieve db connnection: %w", err)
	}

	return db.Close()
}

// DB lets the function to custom actions on the db.
func (r *Repo) DB(f func(db *gorm.DB) error) error {
	if err := f(r.db.Model(r.newEntity())); err != nil {
		return &eh.RepoError{
			Err: err,
		}
	}
	return nil
}
