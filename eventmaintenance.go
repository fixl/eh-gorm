// Copyright (c) 2024 - The Event Horizon GORM authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ehgorm

import (
	"context"
	"errors"
	"fmt"
	eh "github.com/looplab/eventhorizon"
	"gorm.io/gorm"
)

// Replace implements the Replace method of the eventhorizon.EventStoreMaintenance interface
func (s *EventStore) Replace(ctx context.Context, event eh.Event) error {
	id := event.AggregateID()
	at := event.AggregateType()
	av := event.Version()

	fmt.Println(id.String())

	err := s.db.Transaction(func(tx *gorm.DB) error {
		var count int64
		if err := eventsTable(s, tx).Where("aggregate_id = ?", id).Count(&count).Error; err != nil {
			return err
		}

		if count == 0 {
			return eh.ErrAggregateNotFound
		}

		// Create the event record for the database
		e, err := newEvt(ctx, event)
		if err != nil {
			return err
		}

		var eventToReplace evt
		if err = eventsTable(s, tx).Where("aggregate_id = ? AND version = ?", id, av).Take(&eventToReplace).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return eh.ErrEventNotFound
			}
			return fmt.Errorf("could not find original event: %w", err)
		}

		e.Position = eventToReplace.Position
		e.Metadata["position"] = eventToReplace.Position

		return eventsTable(s, tx).Save(&e).Error
	})

	if err != nil {
		return &eh.EventStoreError{
			Err:              err,
			Op:               eh.EventStoreOpReplace,
			AggregateType:    at,
			AggregateID:      id,
			AggregateVersion: av,
			Events:           []eh.Event{event},
		}
	}

	return nil
}

// RenameEvent implements the RenameEvent method of the eventhorizon.EventStoreMaintenance interface
func (s *EventStore) RenameEvent(ctx context.Context, from, to eh.EventType) error {
	if err := eventsTable(s, s.db).Where("event_type = ?", from.String()).Updates(&evt{EventType: to}).Error; err != nil {
		return &eh.EventStoreError{
			Err: fmt.Errorf("could not update events of type '%s': %w", from, err),
			Op:  eh.EventStoreOpRename,
		}
	}
	return nil
}

// Clear clears the event storage
func (s *EventStore) Clear(ctx context.Context) error {
	if err := eventsTable(s, s.db).Migrator().DropTable(&evt{}); err != nil {
		return &eh.EventStoreError{
			Err: fmt.Errorf("could not clear events table: %w", err),
		}
	}

	if err := streamsTable(s, s.db).Migrator().DropTable(&stream{}); err != nil {
		return &eh.EventStoreError{
			Err: fmt.Errorf("could not clear stream table: %w", err),
		}
	}

	if err := snapshotsTable(s, s.db).Migrator().DropTable(&snapshotRecord{}); err != nil {
		return &eh.EventStoreError{
			Err: fmt.Errorf("could not clear snapshot table: %w", err),
		}
	}

	return nil
}
