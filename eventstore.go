// Copyright (c) 2024 - The Event Horizon GORM authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ehgorm

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	eh "github.com/looplab/eventhorizon"
	"github.com/looplab/eventhorizon/uuid"
	"gorm.io/gorm"
	"io"
	"time"
)

// EventStore is an eventhorizon.EventStore for GORM, using one table
// for all events and another to keep track of all aggregates/streams.
// It also keeps track of the global position of events, stored as metadata.
type EventStore struct {
	db                    *gorm.DB
	eventsTable           string
	streamsTable          string
	snapshotsTable        string
	globalStreamID        uuid.UUID
	eventHandlerAfterSave eh.EventHandler
	eventHandlerInTX      eh.EventHandler
}

func drop(store *EventStore) {
	fmt.Println("Dropping store")
	err := store.db.Migrator().DropTable(store.eventsTable, store.streamsTable, store.snapshotsTable)

	if err != nil {
		fmt.Printf("%v", err)
	}
}

// NewEventStore create a new EventStore with a GORM database
func NewEventStore(db *gorm.DB, options ...Option) (*EventStore, error) {
	if db == nil {
		return nil, fmt.Errorf("missing GORM database")
	}

	store := &EventStore{
		db:             db,
		eventsTable:    "events",
		streamsTable:   "streams",
		snapshotsTable: "snapshots",
		globalStreamID: uuid.MustParse("ffffffff-ffff-ffff-ffff-ffffffffffff"),
	}

	for _, option := range options {
		if err := option(store); err != nil {
			return nil, fmt.Errorf("error while applying option: %w", err)
		}
	}

	if err := eventsTable(store, db).AutoMigrate(&evt{}); err != nil {
		return nil, fmt.Errorf("could not migrate events table: %w", err)
	}

	if err := streamsTable(store, db).AutoMigrate(&stream{}); err != nil {
		return nil, fmt.Errorf("could not migrate stream table: %w", err)
	}

	if err := snapshotsTable(store, db).AutoMigrate(&snapshotRecord{}); err != nil {
		return nil, fmt.Errorf("could not migrate snapshot table: %w", err)
	}

	// Ensure all stream exists
	var allStream stream
	err := streamsTable(store, db).Take(&allStream, store.globalStreamID).Error

	if err == nil {
		return store, nil
	}

	if !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, fmt.Errorf("could not find $all stream: %w", err)
	}

	allStream.ID = store.globalStreamID
	allStream.Position = 0

	if err = streamsTable(store, db).Save(&allStream).Error; err != nil {
		return nil, fmt.Errorf("could not create $all stream: %w", err)
	}

	return store, nil
}

func eventsTable(store *EventStore, db *gorm.DB) *gorm.DB {
	return db.Scopes(func(d *gorm.DB) *gorm.DB {
		return d.Table(store.eventsTable)
	})
}

func streamsTable(store *EventStore, db *gorm.DB) *gorm.DB {
	return db.Scopes(func(d *gorm.DB) *gorm.DB {
		return d.Table(store.streamsTable)
	})
}

func snapshotsTable(store *EventStore, db *gorm.DB) *gorm.DB {
	return db.Scopes(func(d *gorm.DB) *gorm.DB {
		return d.Table(store.snapshotsTable)
	})
}

// Option is an option setter used to configure EventStore creation.
type Option func(*EventStore) error

// WithEventHandler adds an event handler that will be called after saving events.
// An example would be to add an event bus to publish events.
func WithEventHandler(h eh.EventHandler) Option {
	return func(store *EventStore) error {
		if store.eventHandlerAfterSave != nil {
			return fmt.Errorf("another event handler is already set")
		}

		if store.eventHandlerInTX != nil {
			return fmt.Errorf("another TX event handler is already set")
		}

		store.eventHandlerAfterSave = h

		return nil
	}
}

// WithEventHandlerInTX adds an event handler that will be called during saving
// of events. An example would be to add an outbox to further process events.
// For an outbox to be atomix it needs to use the same transaction as the save
// operation, which is passed down using the context.
func WithEventHandlerInTX(h eh.EventHandler) Option {
	return func(store *EventStore) error {
		if store.eventHandlerAfterSave != nil {
			return fmt.Errorf("another event handler is already set")
		}

		if store.eventHandlerInTX != nil {
			return fmt.Errorf("another TX event handler is already set")
		}

		store.eventHandlerInTX = h

		return nil
	}
}

// WithEventsTableName uses a different event table name.
// This can be used if there ia a different schema in e.g. postgres.
func WithEventsTableName(tableName string) Option {
	return func(store *EventStore) error {
		store.eventsTable = tableName
		return nil
	}
}

func (s *EventStore) Save(ctx context.Context, events []eh.Event, originalVersion int) error {
	if len(events) == 0 {
		return &eh.EventStoreError{
			Err: eh.ErrMissingEvents,
			Op:  eh.EventStoreOpSave,
		}
	}

	dbEvents := make([]*evt, len(events))
	id := events[0].AggregateID()
	at := events[0].AggregateType()

	// Build all event records, with incrementing versions starting
	// from the original aggregate version
	for i, event := range events {
		// Only accept events belonging to the same aggregate
		if event.AggregateID() != id {
			return &eh.EventStoreError{
				Err:              eh.ErrMismatchedEventAggregateIDs,
				Op:               eh.EventStoreOpSave,
				AggregateType:    at,
				AggregateID:      id,
				AggregateVersion: originalVersion,
				Events:           events,
			}
		}

		if event.AggregateType() != at {
			return &eh.EventStoreError{
				Err:              eh.ErrMismatchedEventAggregateTypes,
				Op:               eh.EventStoreOpSave,
				AggregateType:    at,
				AggregateID:      id,
				AggregateVersion: originalVersion,
				Events:           events,
			}
		}

		// Only accept events that apply to the correct aggregate version.
		if event.Version() != originalVersion+i+1 {
			return &eh.EventStoreError{
				Err:              eh.ErrIncorrectEventVersion,
				Op:               eh.EventStoreOpSave,
				AggregateType:    at,
				AggregateID:      id,
				AggregateVersion: originalVersion,
				Events:           events,
			}
		}

		// Create the event record for the DB.
		e, err := newEvt(ctx, event)
		if err != nil {
			return err
		}

		dbEvents[i] = e
	}

	err := s.db.Transaction(func(tx *gorm.DB) error {
		var globalStream stream

		err := streamsTable(s, tx).Take(&globalStream, s.globalStreamID).Error
		if err != nil {
			return fmt.Errorf("could not retrieve global stream: %w", err)
		}

		var aggregateStream stream
		if originalVersion == 0 {
			aggregateStream = stream{
				ID:            id,
				AggregateType: at,
				Version:       0,
				Position:      0,
			}
		} else {
			if err = streamsTable(s, tx).Where(map[string]interface{}{"id": id, "version": originalVersion}).Take(&aggregateStream).Error; err != nil {
				return fmt.Errorf("could not load aggregate stream: %w", err)
			}
		}

		for i, event := range dbEvents {
			event.Position = globalStream.Position + i + 1
			// Also store the position in the event metadata.
			event.Metadata["position"] = event.Position

			// Use the last event to set the new stream position
			if i == len(dbEvents)-1 {
				aggregateStream.Position = event.Position
				aggregateStream.UpdatedAt = event.Timestamp
				aggregateStream.Version += i + 1
			}
		}

		// Create events
		if err = eventsTable(s, tx).Create(dbEvents).Error; err != nil {
			return fmt.Errorf("could not insert event: %w", err)
		}

		// Update aggregate stream
		if err = streamsTable(s, tx).Save(&aggregateStream).Error; err != nil {
			return fmt.Errorf("could not update stream: %w", err)
		}

		// Update the $all stream
		globalStream.Position += len(dbEvents)
		if err = streamsTable(s, tx).Save(&globalStream).Error; err != nil {
			return fmt.Errorf("could not increment global position: %w", err)
		}

		return nil
	})
	if err != nil {
		return &eh.EventStoreError{
			Err:              err,
			Op:               eh.EventStoreOpSave,
			AggregateType:    at,
			AggregateID:      id,
			AggregateVersion: originalVersion,
			Events:           events,
		}
	}

	// Let the optional event handler handle the events
	if s.eventHandlerAfterSave != nil {
		for _, e := range events {
			if err = s.eventHandlerAfterSave.HandleEvent(ctx, e); err != nil {
				return &eh.EventHandlerError{
					Err:   err,
					Event: e,
				}
			}
		}
	}

	return nil
}

func (s *EventStore) Load(ctx context.Context, id uuid.UUID) ([]eh.Event, error) {
	var events []*evt
	if err := eventsTable(s, s.db).Where("aggregate_id = ?", id).Order("version ASC").Find(&events).Error; err != nil {
		return nil, &eh.EventStoreError{
			Err:         fmt.Errorf("could not find event: %w", err),
			Op:          eh.EventStoreOpLoad,
			AggregateID: id,
		}
	}

	return s.loadFromDbEvents(ctx, id, events)
}

func (s *EventStore) LoadFrom(ctx context.Context, id uuid.UUID, version int) ([]eh.Event, error) {
	var events []*evt
	if err := eventsTable(s, s.db).Where("aggregate_id = ? AND version >= ?", id, version).Order("version ASC").Find(&events).Error; err != nil {
		return nil, &eh.EventStoreError{
			Err:         fmt.Errorf("could not find event: %w", err),
			Op:          eh.EventStoreOpLoad,
			AggregateID: id,
		}
	}

	return nil, nil
}

func (s *EventStore) loadFromDbEvents(ctx context.Context, id uuid.UUID, dbEvents []*evt) ([]eh.Event, error) {
	var events []eh.Event

	for _, e := range dbEvents {
		// Create an event of the correct type
		if len(e.RawData) > 0 {
			var err error
			if e.data, err = eh.CreateEventData(e.EventType); err != nil {
				return nil, &eh.EventStoreError{
					Err:              fmt.Errorf("could not create event data: %w", err),
					Op:               eh.AggregateStoreOpLoad,
					AggregateType:    e.AggregateType,
					AggregateID:      id,
					AggregateVersion: e.Version,
					Events:           events,
				}
			}

			if err := json.Unmarshal(e.RawData, e.data); err != nil {
				return nil, &eh.EventStoreError{
					Err:              fmt.Errorf("could not unmarshal event data: %w", err),
					Op:               eh.EventStoreOpLoad,
					AggregateType:    e.AggregateType,
					AggregateID:      id,
					AggregateVersion: e.Version,
					Events:           events,
				}
			}

			e.RawData = nil
		}

		event := eh.NewEvent(
			e.EventType,
			e.data,
			e.Timestamp,
			eh.ForAggregate(
				e.AggregateType,
				e.AggregateID,
				e.Version,
			),
			eh.WithMetadata(e.Metadata),
		)

		events = append(events, event)
	}

	if len(events) == 0 {
		return nil, &eh.EventStoreError{
			Err:         eh.ErrAggregateNotFound,
			Op:          eh.EventStoreOpLoad,
			AggregateID: id,
		}
	}

	return events, nil
}

func (s *EventStore) LoadSnapshot(ctx context.Context, id uuid.UUID) (*eh.Snapshot, error) {
	var record snapshotRecord
	if err := snapshotsTable(s, s.db).Take(&record, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, nil
		}

		return nil, fmt.Errorf("could not load snapshot record: %w", err)
	}

	var (
		snapshot = new(eh.Snapshot)
		err      error
	)

	if snapshot.State, err = eh.CreateSnapshotData(record.AggregateID, record.AggregateType); err != nil {
		return nil, &eh.EventStoreError{
			Err:         fmt.Errorf("could not create snapshot data: %w", err),
			Op:          eh.EventStoreOpLoadSnapshot,
			AggregateID: id,
		}
	}

	if err = record.decompress(); err != nil {
		return nil, &eh.EventStoreError{
			Err:         fmt.Errorf("could not decompress snapshot: %w", err),
			Op:          eh.EventStoreOpLoadSnapshot,
			AggregateID: id,
		}
	}

	if err = json.Unmarshal(record.RawData, snapshot); err != nil {
		return nil, &eh.EventStoreError{
			Err:         fmt.Errorf("could not unmarshal snapshot record: %w", err),
			Op:          eh.EventStoreOpLoadSnapshot,
			AggregateID: id,
		}
	}

	return snapshot, nil
}

func (s *EventStore) SaveSnapshot(ctx context.Context, id uuid.UUID, snapshot eh.Snapshot) error {
	if snapshot.AggregateType == "" {
		return &eh.EventStoreError{
			Err:           fmt.Errorf("aggregate type is empty"),
			Op:            eh.EventStoreOpSaveSnapshot,
			AggregateID:   id,
			AggregateType: snapshot.AggregateType,
		}
	}

	if snapshot.State == nil {
		return &eh.EventStoreError{
			Err:           fmt.Errorf("snapshots state is nil"),
			Op:            eh.EventStoreOpSaveSnapshot,
			AggregateID:   id,
			AggregateType: snapshot.AggregateType,
		}
	}

	record := snapshotRecord{
		AggregateID:   id,
		AggregateType: snapshot.AggregateType,
		Timestamp:     time.Now(),
		Version:       snapshot.Version,
	}

	var err error
	if record.RawData, err = json.Marshal(snapshot); err != nil {
		return fmt.Errorf("couild not marshal snapshot: %w", err)
	}

	if err = record.compress(); err != nil {
		return &eh.EventStoreError{
			Err:         fmt.Errorf("could not compress snapshot: %w", err),
			Op:          eh.EventStoreOpSaveSnapshot,
			AggregateID: id,
		}
	}

	if err = snapshotsTable(s, s.db).Save(&record).Error; err != nil {
		return &eh.EventStoreError{
			Err:         fmt.Errorf("could not save snapshot: %w", err),
			Op:          eh.EventStoreOpSaveSnapshot,
			AggregateID: id,
		}
	}

	return nil
}

func (s *EventStore) Close() error {
	db, err := s.db.DB()
	if err != nil {
		return fmt.Errorf("could not retrieve db connnection: %w", err)
	}

	return db.Close()
}

// stream is a stream of events, often containing events from an aggregate
type stream struct {
	ID            uuid.UUID `gorm:"primaryKey"`
	Position      int
	AggregateType eh.AggregateType
	Version       int
	UpdatedAt     time.Time
}

// evt is the internal event record for the GORM event store used
// to save and laod events from the DB.
type evt struct {
	Position      int `gorm:"primaryKey"`
	EventType     eh.EventType
	Timestamp     time.Time
	AggregateType eh.AggregateType
	AggregateID   uuid.UUID `gorm:"index:idx_aggregate,priority:1"`
	Version       int       `gorm:"index:idx_aggregate,priority:2"`
	RawData       []byte
	data          eh.EventData           `gorm:"-:all"`
	Metadata      map[string]interface{} `gorm:"serializer:json"`
}

// newEvt returns a new evt from an event.
func newEvt(_ context.Context, event eh.Event) (*evt, error) {
	e := &evt{
		EventType:     event.EventType(),
		Timestamp:     event.Timestamp(),
		AggregateType: event.AggregateType(),
		AggregateID:   event.AggregateID(),
		Version:       event.Version(),
		Metadata:      event.Metadata(),
	}

	if e.Metadata == nil {
		e.Metadata = map[string]interface{}{}
	}

	// Marshal event data if there is any
	if event.Data() != nil {
		var err error
		e.RawData, err = json.Marshal(event.Data())
		if err != nil {
			return nil, &eh.EventStoreError{
				Err: fmt.Errorf("could not marshal event data: %w", err),
			}
		}
	}

	return e, nil
}

type snapshotRecord struct {
	AggregateID   uuid.UUID `gorm:"primaryKey"`
	RawData       []byte
	Timestamp     time.Time
	Version       int
	AggregateType eh.AggregateType
}

func (s *snapshotRecord) decompress() error {
	byteReader := bytes.NewReader(s.RawData)
	reader, err := gzip.NewReader(byteReader)

	if err != nil {
		return err
	}

	s.RawData, err = io.ReadAll(reader)

	return err
}

func (s *snapshotRecord) compress() error {
	var b bytes.Buffer
	w := gzip.NewWriter(&b)

	if _, err := w.Write(s.RawData); err != nil {
		return err
	}

	if err := w.Flush(); err != nil {
		return err
	}

	if err := w.Close(); err != nil {
		return err
	}

	s.RawData = b.Bytes()

	return nil
}
